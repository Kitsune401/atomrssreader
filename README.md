﻿# Atom RSS Reader

Mini Projet de Web dynamique ayant pour objectif de créer une application web en Python de lecteur de flux RSS/Atom


# Installation

Nécessite Python 3 et Pipenv.

## Librairies à installer dans pipenv

- feedparser
- flask
- peewee
- flask-login
- flask-wtf
- wtf-peewee
- requests

# Utilisateurs de test
Utilisateur 1 :
Username = Samy
Password = Jurassic

Utilisateur 2 :
Username = Albain
Password = Crepe

# Auteurs
Albain LAURENT - Adrien NICOLLE
