from flask import Flask, render_template, session, flash, redirect, url_for
from flask_login import LoginManager, current_user, login_user, login_required, logout_user
from models import User, Feed, create_tables, drop_tables
from forms import LoginForm, RegisterForm, FeedForm
from werkzeug.security import generate_password_hash, check_password_hash
from feedparser import parse
import click, requests, re

""""Initialisation et Fonctions"""

#Utilise du regex pour nettoyer les balises html superflues
def cleanhtml(raw_html):
  cleanr = re.compile('<.*?>')
  cleantext = re.sub(cleanr, '', raw_html)
  return cleantext

#Initialisation de l'application
app = Flask(__name__)
app.secret_key = b'kolczkockaobrnbr'
app.jinja_env.globals.update(cleanhtml=cleanhtml)

login = LoginManager(app)
login.login_view = 'login'

@login.user_loader
def load_user(id):
    return User.get(int(id))

#Génère la version hashée d'un mot de passe
def hashPassword(password):
    return generate_password_hash(password)

#Vérifie le mot de passe
def checkPassword(hashedPassword, password):
    return check_password_hash(hashedPassword, password)

#Récupère le contenu des flux de syndication de l'utilisateur
def getUserFeeds():
    userID = current_user.id
    feedsURL = Feed.select().where(Feed.user == userID)
    feedsData = []

    for url in feedsURL:
        feed = parse(url.url)
        for entry in feed.entries:
            feedsData.append(entry)

    return feedsData

"""Routes"""
#Route index, avec les flux
@app.route('/')
@app.route('/index')
@login_required
def index():

    feedsData = getUserFeeds()
    return render_template('index.html', feeds = feedsData)

#Route gérant la connexion
@app.route('/login', methods=['GET', 'POST'])
def login():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    form = LoginForm()
    if form.validate_on_submit():
        try:
            user = User.get(User.username == form.username.data)
            if not checkPassword(user.password, form.password.data):
                flash('Invalid credentials')
                return redirect(url_for('login'))
            login_user(user)
            flash('Successfully logged in')
            return redirect(url_for('index'))
        except Exception:
            flash('Invalid credentials')
            return redirect(url_for('login'))

    return render_template('users/loginForm.html', form=form)

#Route gérant la déconnexion
@app.route('/logout')
@login_required
def logout():
    logout_user()
    flash('Successfully logged out')
    return redirect(url_for('login'))

#Route gérant la création d'un nouveau compte
@app.route('/register', methods=['GET', 'POST'])
def register():

    if current_user.is_authenticated:
        return redirect(url_for('index'))

    user = User()
    form = RegisterForm()

    if form.validate_on_submit():
        if not User.select().where(User.username == form.username.data):
            form.populate_obj(user)
            user.password = hashPassword(form.password.data)
            user.save()
            flash("Successfully registered")
            return redirect(url_for('login'))
        else:
            flash("This user already exists")
            return redirect(url_for('register'))

    return render_template('users/registerForm.html', form=form)

#Route gérant l'ajout d'un nouveau flux de syndication
@app.route('/addfeed', methods=['GET', 'POST'])
@login_required
def addfeed():
    feed = Feed()
    form = FeedForm()

    if form.validate_on_submit():
        form.populate_obj(feed)
        feed.user = current_user.id
        feed.save()
        flash('Successfully added')
        return redirect(url_for('index'))

    return render_template('feeds/feedsForm.html', form=form)

#Gestion de la base de donnée en ligne de commande
@app.cli.command()
def initdb():
    """Create database"""
    create_tables()
    click.echo('Initialized the database')

@app.cli.command()
def dropdb():
    """Drop database tables"""
    drop_tables()
    click.echo('Dropped tables from database')
