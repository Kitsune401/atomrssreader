from flask_wtf import FlaskForm
from wtforms import StringField, SelectField, PasswordField
from wtforms.validators import DataRequired, Length
from models import User, Feed

class LoginForm(FlaskForm):
    username = StringField('Username ', validators=[DataRequired()])
    password = PasswordField('Password ', validators=[DataRequired()])

class RegisterForm(FlaskForm):
    username = StringField('Username ', validators=[DataRequired(), Length(min=3, max=16)])
    password = PasswordField('Password ', validators=[DataRequired(), Length(min=3, max=16)])

class FeedForm(FlaskForm):
    url = StringField('URL ', validators=[DataRequired()])
