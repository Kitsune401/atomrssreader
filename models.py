from peewee import *
from flask_login import UserMixin

sqlite_db = SqliteDatabase("users.sqlite3")

class BaseModel(Model):

    class Meta:
        database = sqlite_db

class User(UserMixin, BaseModel):
    username = CharField(unique=True)
    password = CharField()

class Feed(BaseModel):
    url = CharField()
    user = ForeignKeyField(User, backref="users")


def create_tables():
    with sqlite_db:
        sqlite_db.create_tables([User, Feed])

def drop_tables():
    with sqlite_db:
        sqlite_db.drop_tables([User, Feed])
